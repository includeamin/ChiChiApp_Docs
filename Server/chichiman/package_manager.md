PORT : 3003

```
URL /package/<chichi_man_id>
METHOD GET
HEADER Id Token
BODY Name
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : there is no assigned package to specified chichi man
```

```
URL /package/state/received
METHOD POST
HEADER Id Token
BODY ChiChiManId PackageNumber
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : package not found
```

```
URL /package/state/checked
METHOD POST
HEADER Id Token
BODY ChiChiManId PackageNumber
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : package not found
```

```
URL /package/state/going
METHOD POST
HEADER Id Token
BODY ChiChiManId PackageNumber
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : package not found
```

```
URL /package/state/arrived
METHOD POST
HEADER Id Token
BODY ChiChiManId PackageNumber
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : package not found
```

```
URL /package/state/delivered
METHOD POST
HEADER Id Token
BODY ChiChiManId PackageNumber DeliveryDuration
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : package not found
```

```
URL /package/state/end
METHOD POST
HEADER Id Token
BODY ChiChiManId PackageNumber Latitude Longitude TurnAroundTime
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : package not found
```

```
URL /package/feedback
METHOD POST
HEADER Id Token
BODY ChiChiManId PackageNumber WarehouseProblems CustomerProblems Rate
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : package not found
```