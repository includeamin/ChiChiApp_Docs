# Sample
PORT : 0000
```
URL /sample
METHOD POST
HEADER Id Token
BODY Name
RESPONSE :
 - Success : d
ERRORS :
 -  Not Allowed : user access denied
 -  Item Not Found : user not found
```

```
URL /chichiman/register
METHOD POST
HEADER 
BODY PhoneNumber
RESPONSE :
 - Success : d
ERRORS :
 -  Not Allowed : phone number is not valid
 -  Item Not Found : could not send activation code
 -  Item Already Exists: phone number is duplicated
```

```
URL /chichiman/verify/<phonenumber>/<code>
METHOD GET
HEADER 
BODY 
RESPONSE :
 - Success : d
ERRORS :
 -  Not Allowed : phone number or actuvatin code is not valid
 -  Item Not Found : user not found
```

```
URL /chichiman/info/personal
METHOD POST
HEADER 
BODY  PhoneNumber FirstName LastName Serial ProfilePic Birthday DriverLicense 
      Address MartialStatus Sex WorkExperience* FatherName* placeOfIssue* HomePhone*
RESPONSE :
 - Success : d
ERRORS :
 -  Not Allowed : some of the mandatory body arguments are not valid
    or user is not verified
```

```
URL /chichiman/info/delivery
METHOD POST
HEADER 
BODY  PhoneNumber DeliveryType PlateNumber CardNumber VehicleModel* VehicleColor*
RESPONSE :
 - Success : d
ERRORS :
 -  Not Allowed : some of the mandatory body arguments are not valid
                  or user did not complete his personal info yet
 -  Item Already Exists: plate number is duplicated
```

```
URL /chichiman/info/contract
METHOD POST
HEADER 
BODY  PhoneNumber Image Status SSNCardImage BasePayment 
      DeliveryLicenseImage EndOfContract AccountNumber
RESPONSE :
 - Success : d
ERRORS :
 -  Not Allowed : some of the mandatory body arguments are not valid
                  or user did not complete his delivery info yet
 -  Item Already Exists: plate number is duplicated
```

```
URL /chichiman/send/message
METHOD POST
HEADER 
BODY  PhoneNumber
RESPONSE :
 - Success : d
ERRORS :
 -  Not Allowed : phone number is not valid
                  or user did not complete his contract info yet
```
