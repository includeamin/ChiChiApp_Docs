# Sample
PORT : 0000
```
URL /sample
METHOD POST
HEADER Id Token
BODY Name
RESPONSE :
 - Success : d
ERRORS :
 -  Not Allowed : user access denied
 -  Item Not Found : user not found
```

```
URL /users/signup
METHOD POST
BODY PhoneNumber FirstName LastName InvitationCode*
RESPONSE :
 - Success : d
ERRORS :
 -  Not Allowed : firstname or lastname or phone number is not valid
 -  Item Not Found : Invitation code can not be found
 -  Item Already Exists: phone number is duplicated
```

```
URL /users/phonenumber/verification/<phonenumber>/<code>
METHOD GET
RESPONSE :
 - Success : Id Token
ERRORS :
 -  Not Allowed : activation code or phone number is not valid
 -  Item Not Found : user not found
 -  Failed To Generate Token: error during token generation
```

```
URL /users/login
METHOD POST
BODY PhoneNumber
RESPONSE :
 - Success : d
ERRORS :
 -  Not Allowed : phone number is not valid
 -  Item Not Found : user not found
```

```
URL /users/login/verification
METHOD POST
BODY PhoneNumber VerificationCode
RESPONSE :
 - Success : Id Token
ERRORS :
 -  Not Allowed : verification code or phone number is not valid
 -  Item Not Found : user not found
 -  Failed To Generate Token: error during token generation
```

```
URL /users/logout
METHOD POST
BODY
HEADERS Id Token
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : user not found
```

```
URL /users/information/update
METHOD POST
BODY PhoneNumber FirstName LastName NickName Email
HEADERS Id Token
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : user not found
```

```
URL /users/email/verification
METHOD POST
BODY Email Code
HEADERS Id Token
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : user not found
 -  Not Allowed: email or activation code is not valid
```

```
URL /users/locations/add
METHOD POST
BODY PhoneNumber Name GEO Address
HEADERS Id Token
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : user not found
 -  Not Allowed: GEO or Address is not valid
```

```
URL /users/locations/delete
METHOD POST
BODY Name
HEADERS Id Token
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : user not found
```

```
URL /users/locations
METHOD GET
BODY
HEADERS Id Token
RESPONSE :
 - Success : {
                State,
                Description: {
                    Locations: [
                       Address: {
                          City,
                          PostalCode
                          Street
                       },
                       Geo: {
                          Lang,
                          Lat
                       },
                       Name
                    ]
                }
           }
ERRORS :
 -  Item Not Found : user not found
```

```
URL /users/friends/invitation
METHOD POST
BODY PhoneNumber
HEADERS Id Token
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : user not found
 -  Not Allowed : phone number is not valid
 -  Item Already Exists: invited friend is already in the database
```

```
URL /users/info/email/is_verified
METHOD POST
BODY Email
HEADERS Id Token
RESPONSE :
 - Success : Is_Verified
ERRORS :
 -  Item Not Found : user not found
 -  Not Allowed : Email is not valid
```

```
URL /users/info/email/code/resend
METHOD POST
BODY Email
HEADERS Id Token
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : user not found
 -  Not Allowed : Email is not valid or user is not confirmed or user does not own the email
```

```
URL /users/info/email/code/resend
METHOD POST
BODY Email
HEADERS Id Token
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : user not found
 -  Not Allowed : Email is not valid or user is not confirmed or user does not own the email
```

```
URL /users/info/phonenumber/code/resend
METHOD POST
BODY PhoneNumber
HEADERS Id Token
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : user not found
 -  Not Allowed : PhoneNumber is not valid
```

```
URL /users/info/email/add
METHOD POST
BODY Email
HEADERS Id Token
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : user not found
 -  Not Allowed : user is not confirmed or registered
 -  Item Already Exists: email is not unique
```
