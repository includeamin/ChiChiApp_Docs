```
URL /admin/product/add
METHOD POST
BODY Name Attribue Brand PrevPrice CurrentPrice Description Images
RESPONSE :
 - Success : d
ERRORS :
 -  Item Already Exists: combination of (Name, Attribute, Brand) is not unique
```

```
URL /admin/product/update
METHOD POST
BODY ProductId Name Attribue Brand PrevPrice CurrentPrice Description Images 
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : product not found
 -  Item Already Exists: combination of (Name, Attribute, Brand) is not unique
```

```
URL /admin/product/delete
METHOD POST
BODY ProductId
RESPONSE :
 - Success : d
ERRORS :
 -  Item Not Found : product not found
```

```
URL /admin/product/all
METHOD GET
BODY 
RESPONSE :
 - Success : {
	 	State,
		Description: [
			{
				_id,
				Attribute
				Brand
				CurrentPrice
				PrevPrice
				Description
				Images
				Name
			}
		]
	 }
ERRORS :
```

```
URL /admin/product/<product_id>
METHOD GET
BODY
RESPONSE :
 - Success : {
	 	State,
		Description: {
			_id,
			Attribute
			Brand
			CurrentPrice
			PrevPrice
			Description
			Images
			Name
		}
	}
ERRORS :
```
