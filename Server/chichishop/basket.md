# Sample
PORT : 0000
```
URL /sample
METHOD POST
HEADER Id Token
BODY Name
RESPONSE :
 - Success : d
ERRORS :
 -  Not Allowed : user access denied
 -  Item Not Found : user not found
```

```
URL /basket/add
METHOD POST
BODY UserId ProductId PrevPrice CurrentPrice Image
RESPONSE:
 - Success: d
ERRORS :
-   Item Not Found: product not found
```

```
URL /basket/delete
METHOD POST
BODY UserId ProductId 
RESPONSE:
 - Success: d
ERRORS :
-   Item Not Found: product not found
```

```
URL /basket/<user_id>
METHOD GET
BODY 
RESPONSE:
 - Success: {
	 State,
	 UserId,
	 Description: {
		 Image,
		 IsConfirmed
	 	 Products: [{
			 Count,
			 CurrentPrice,
			 PrevPrice,
			 ProductId

			 }]

		 }


	 }
ERRORS :
-   Item Not Found: basket not found
```

```
URL /basket/confirm
METHOD POST
BODY UserId
RESPONSE:
 - Success: d 
ERRORS :
-   Item Not Found: basket not found
```

```
URL /basket/undoconfirm
METHOD POST
BODY UserId
RESPONSE:
 - Success: d 
ERRORS :
-   Item Not Found: basket not found
```
